<?php
/**
 * @file
 *   Project export_ui plugin.
 */

/**
 * Overrides for l10n server select Projects in export UI.
 */
class l10n_server_select_projects extends ctools_export_ui {

  /**
   * Initialize the plugin with some modifications.
   */
  function init($plugin) {
    $plugin['menu']['items']['list callback']['type'] = MENU_LOCAL_TASK;

    parent::init($plugin);
  }

  /**
   * Override menu items.
   */
  function hook_menu(&$items) {
    $stored_items = $this->plugin['menu']['items'];

    parent::hook_menu($items);

    $this->plugin['menu']['items'] = $stored_items;
  }

  /**
   * Provide the actual editing form.
   */
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);

    $item = $form_state['item'];
    $form['info']['name']['#description'] .= ' ' . t('Often the Drupal-project system name.');
    $form['info']['name']['#required'] = TRUE;
    $form['info']['name']['#maxlength'] = 255;

    $form['project'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => $item->project,
      '#options' => variable_get('l10n_server_projects', array(
        'module' => 'Module',
        'theme' => 'Theme'
      )),
      '#empty_option' => t('- Select -'),
      '#description' => t(''),
      '#required' => TRUE,
    );

    $form['core'] = array(
      '#type' => 'textfield',
      '#title' => t('Core'),
      '#maxlength' => 20,
      '#size' => 25,
      '#default_value' => is_null($item->core) ? '7.x' : $item->core,
      '#description' => t('Specify a core version, eg. 7.x'),
      '#required' => TRUE,
    );

    $form['version'] = array(
      '#type' => 'textfield',
      '#title' => t('Version'),
      '#maxlength' => 20,
      '#size' => 25,
      '#default_value' => $item->version,
      '#description' => t('Specify a package version, eg. 7.x-1.0'),
      '#required' => TRUE,
    );

    $servers = ctools_export_crud_load_all('l10n_server_select_servers');

    $form['server'] = array(
      '#type' => 'select',
      '#title' => t('Server'),
      '#default_value' => $item->server,
      '#options' => drupal_map_assoc(array_keys($servers)),
      '#empty_option' => t('- Select -'),
      '#required' => TRUE,
    );
  }

  public function list_form(&$form, &$form_state) {
    parent::list_form($form, $form_state);
    $form['bottom row']['#access'] = FALSE;
    $form['actions'] = array(
      '#type' => 'actions',
    );
    $form['actions']['submit'] = $form['bottom row']['submit'];
    $form['actions']['reset'] = $form['bottom row']['reset'];
  }

  /**
   * Rebuild the row with extra data.
   */
  public function list_build_row($item, &$form_state, $operations) {
    $order = tablesort_get_order($this->list_table_header());

    if (isset($sort_query['order'], $sort_query['sort'])) {
      $form_state['values']['order'] = $sort_query['order'];
      $form_state['values']['sort'] = $sort_query['sort'];
    }

    parent::list_build_row($item, $form_state, $operations);
    $name = $item->{$this->plugin['export']['key']};

    switch ($order) {
      case 'version':
        $this->sorts[$name] = $item->version . $name;
        break;
      case 'project':
        $this->sorts[$name] = $item->project . $name;
        break;
      case 'server':
        $this->sorts[$name] = $item->server . $name;
        break;
    }

    $data = $this->rows[$name]['data'];
    $this->rows[$name]['data'] = array(array_shift($data));

    $new = array(
      array('data' => check_plain($item->core . '-' . $item->version)),
      array('data' => check_plain($item->project)),
      array('data' => check_plain($item->name)),
    );

    $this->rows[$name]['data'] = array_merge($this->rows[$name]['data'], $new, $data);
  }

  /**
   * Rebuild the table header with sorting options and extra headers.
   *
   * @return array
   */
  public function list_table_header() {
    $parent = parent::list_table_header();
    $header = array();

    // Look like Views, got it from Views.
    $name = array_shift($parent);
    $header[] = $name + array('field' => 'name', 'order' => 'ASC');
    $header[] = array(
      'data' => t('Version'),
      'field' => 'version',
      'order' => 'ASC'
    );
    $header[] = array(
      'data' => t('Project'),
      'field' => 'project',
      'order' => 'ASC'
    );
    $header[] = array(
      'data' => t('Server'),
      'field' => 'server',
      'order' => 'ASC'
    );
    $header = array_merge($header, $parent);

    return $header;
  }

  /**
   * Render list with the sorting values.
   *
   * @param array $form_state The actual form state.
   * @return string
   */
  public function list_render(&$form_state) {
    $sort = tablesort_get_sort($this->list_table_header());

    // Now actually sort
    if ($sort == 'desc') {
      arsort($this->sorts);
    }
    else {
      asort($this->sorts);
    }

    // Nuke the original.
    $rows = $this->rows;
    $this->rows = array();
    // And restore.
    foreach ($this->sorts as $name => $title) {
      $this->rows[$name] = $rows[$name];
    }

    return parent::list_render($form_state);
  }
}
