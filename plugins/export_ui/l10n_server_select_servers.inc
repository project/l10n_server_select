<?php
/**
 * @file
 *   Server export_ui plugin.
 */

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'title' => 'l10n server select servers',
  'schema' => 'l10n_server_select_servers',
  'menu' => array(
    'menu prefix' => 'admin/config/regional/language/update',
    'menu item' => 'servers',
    'menu title' => 'Servers',
    'menu description' => 'Custom l10n Server settings.',
  ),
  // Define user interface texts.
  'title singular' => t('Server'),
  'title plural' => t('Servers'),
  'title singular proper' => t('Server'),
  'title plural proper' => t('Servers'),
  'handler' => array(
    'class' => 'l10n_server_select_servers',
    'parent' => 'ctools_export_ui',
  ),
);
