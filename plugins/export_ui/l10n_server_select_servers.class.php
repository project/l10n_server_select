<?php

/**
 * Overrides for l10n server select Servers in export UI.
 */
class l10n_server_select_servers extends ctools_export_ui {

  /**
   * Initialize the plugin with some modifications.
   */
  function init($plugin) {
    $plugin['menu']['items']['list callback']['type'] = MENU_LOCAL_TASK;

    return parent::init($plugin);
  }

  /**
   * Override menu items.
   */
  function hook_menu(&$items) {
    $stored_items = $this->plugin['menu']['items'];

    parent::hook_menu($items);

    $this->plugin['menu']['items'] = $stored_items;
  }

  /**
   * Provide the actual editing form.
   */
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);

    $item = $form_state['item'];

    $form['info']['name']['#required'] = TRUE;
    $form['info']['name']['#maxlength'] = 255;

    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('l10n Path'),
      '#required' => TRUE,
      '#default_value' => $item->path,
      '#description' => t('Specify the package download path.<br />eg. http://example.com/files/translations/%core/%project/%project-%release.%language.po'),
    );
  }
}