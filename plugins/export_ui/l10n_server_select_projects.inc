<?php
/**
 * @file
 *   Project export_ui plugin.
 */

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'title' => 'l10n server select projects',
  'schema' => 'l10n_server_select_projects',
  'menu' => array(
    'menu prefix' => 'admin/config/regional/language/update',
    'menu item' => 'projects',
    'menu title' => 'Projects',
    'menu description' => 'Custom l10n server settings per Project.',
  ),
  // Define user interface texts.
  'title singular' => t('Project'),
  'title plural' => t('Projects'),
  'title singular proper' => t('Project'),
  'title plural proper' => t('Projects'),
  'handler' => array(
    'class' => 'l10n_server_select_projects',
    'parent' => 'ctools_export_ui',
  ),
);
